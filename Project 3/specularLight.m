%View Project
%Author: Papas Ioannis 9218

function I=specularLight(P,N,C,ks,ncoeff,S,I0)
% I=specularLight(P,N,C,ks,ncoeff,S,I0) calculates the specular light intensity
% INPUTS:
% P: 3x1 vector with the point coordinates
% N: 3x1 vector with the coordinates of the normal vector of the point
% C: 3x1 vector with the point of viewer coordinates
% ks: 3x1 vector with the specular light coefficients
% ncoeff: scalar indicating the phong coefficient
% S: 3xN matrix with the coordinates of every source
% I0: 3x1 vector with the intensity of the source
% OUTPUTS:
% I: 3x1 vector with the intensity at a point P

V=(C-P)/norm(C-P);
n=length(S(1,:)); %number of sources
I=zeros(3,1);
for iN=1:n
    L=(S(:,iN)-P)/norm(S(:,iN)-P);
    R=2*N*dot(N,L)-L;
    phong=(max((dot(R,V))^ncoeff,0));
    I=(I0.*ks.*phong)+I;
end


end