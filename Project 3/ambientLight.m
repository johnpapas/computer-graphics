%View Project
%Author: Papas Ioannis 9218

function I = ambientLight(ka,Ia)
% I = ambientLight(ka,Ia) calculates the ambient light intensity
% INPUTS:
% ka: 3x1 vector with the ambient light coefficients
% Ia: 3x1 vector with the intensity of the source
% OUTPUTS:
% I: 3x1 vector with the intensity at a point P


I=ka.*Ia;


end
