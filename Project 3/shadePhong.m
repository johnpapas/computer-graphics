%View Project
%Author: Papas Ioannis 9218

function Y = shadePhong(p,Vn,Pc,C,S,ka,kd,ks,ncoeff,Ia,I0,X)

%Check if we dont have any triangle

if isempty(p)
    return;
end

%Variables
Y=X;
xV=p(1,:)';
yV=p(2,:)';

%%
%Check if triangle is a horizontal line

if (yV(1)==yV(2) && yV(1)==yV(3))
    for x=min(xV):1:max(xV)
    	normals=interp(Vn(:,1),Vn(:,2),min(xV),max(xV),x);
        KA=interp(ka(:,1),ka(:,2),min(xV),max(xV),x);
        KD=interp(kd(:,1),kd(:,2),min(xV),max(xV),x);
        KS=interp(ks(:,1),ks(:,2),min(xV),max(xV),x);
        ambientI=ambientLight(KA,Ia);
        specularI=specularLight(Pc,normals,C,KS,ncoeff,S,I0);
        diffuseI=diffuseLight(Pc,normals,KD,S,I0);
        Y(yV(1),x,:)=ambientI+specularI+diffuseI;
    end
    return;
end

%%
%Check if triangle is a vertical line

if (xV(1)==xV(2) && xV(1)==xV(3))
    for y=min(yV):1:max(yV)
    	normals=interp(Vn(:,1),Vn(:,2),min(yV),max(yV),y);
        KA=interp(ka(:,1),ka(:,2),min(yV),max(yV),y);
        KD=interp(kd(:,1),kd(:,2),min(yV),max(yV),y);
        KS=interp(ks(:,1),ks(:,2),min(yV),max(yV),y);
        ambientI=ambientLight(KA,Ia);
        specularI=specularLight(Pc,normals,C,KS,ncoeff,S,I0);
        diffuseI=diffuseLight(Pc,normals,KD,S,I0);
        Y(y,xV(1),:)=ambientI+specularI+diffuseI;
    end
    return;
end

%%
%Filling Algorithm

ykminV=[min([yV(1) yV(2)]),min([yV(2) yV(3)]),min([yV(1) yV(3)])];
ykmaxV=[max([yV(1) yV(2)]),max([yV(2) yV(3)]),max([yV(1) yV(3)])];

ymin=min(ykminV);
ymax=max(ykmaxV);

activeEdges=false(3,1);

mKV=NaN(3,1);
mKV(1)=(yV(1)-yV(2))/(xV(1)-xV(2));
mKV(2)=(yV(2)-yV(3))/(xV(2)-xV(3));
mKV(3)=(yV(1)-yV(3))/(xV(1)-xV(3));

bV=yV-mKV.*xV;

%Check if the triangle has an horizontal edge and draw it

%%
%The horizontal edge is in the lower place
if((yV(1)==yV(2) && yV(1)==ymin) || (yV(1)==yV(3) && yV(1)==ymin) || (yV(2)==yV(3) && yV(2)==ymin))
    xxV=xV(yV==ymin);
    normalsV=Vn(:,(yV==ymin));
    KAV=ka(:,(yV==ymin));
    KSV=ks(:,(yV==ymin));
    KDV=kd(:,(yV==ymin));
    [xxV,index]=sort(xxV);
    
    %Draw the horizontal edge
    
    for x=min(xxV):max(xxV)
        normals1=interp(normalsV(:,index(1)),normalsV(:,index(2)),xxV(1),xxV(2),x);
        ka1=interp(KAV(:,index(1)),KAV(:,index(2)),xxV(1),xxV(2),x);
        kd1=interp(KDV(:,index(1)),KDV(:,index(2)),xxV(1),xxV(2),x);
        ks1=interp(KSV(:,index(1)),KSV(:,index(2)),xxV(1),xxV(2),x);
        ambientI=ambientLight(ka1,Ia);
        specularI=specularLight(Pc,normals1,C,ks1,ncoeff,S,I0);
        diffuseI=diffuseLight(Pc,normals1,kd1,S,I0);
        Y(ymin,x,:)=ambientI+specularI+diffuseI;
    end
    ymin=ymin+1;
    for y=ymin:ymax-1
        
        %Find the active edges
        
        activeEdges(:)=(y>=ykminV & y<ykmaxV);
        
        %Find the active limit points
        
        limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
        mActiveKV=mKV(activeEdges);
        
        %Check if an edge is vertical (mK==Inf)
        
        if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
            [~,ia]=unique(xV,'stable');
            sameX=false(size(xV));
            sameX(ia)=true;
            limitPointsV(isnan(limitPointsV))=xV(~sameX);
        end
        
        %Measure the coefficients and normals that we will use at the interpolation
        
        normals1=interp(normalsV(:,index(2)),Vn(:,yV==ymax),ymin,ymax,y);
        normals2=interp(normalsV(:,index(1)),Vn(:,yV==ymax),ymin,ymax,y);
        KA1=interp(KAV(:,index(2)),ka(:,yV==ymax),ymin,ymax,y);
        KA2=interp(KAV(:,index(1)),ka(:,yV==ymax),ymin,ymax,y);
        KD1=interp(KDV(:,index(2)),kd(:,yV==ymax),ymin,ymax,y);
        KD2=interp(KDV(:,index(1)),kd(:,yV==ymax),ymin,ymax,y);
        KS1=interp(KSV(:,index(2)),ks(:,yV==ymax),ymin,ymax,y);
        KS2=interp(KSV(:,index(1)),ks(:,yV==ymax),ymin,ymax,y);
        
        
        %Draw every pixel of the current scanline
        
        for x=min(limitPointsV):max(limitPointsV)
            normals=interp(normals1,normals2,max(limitPointsV),min(limitPointsV),x);
            KA=interp(KA1,KA2,max(limitPointsV),min(limitPointsV),x);
            KD=interp(KD1,KD2,max(limitPointsV),min(limitPointsV),x);
            KS=interp(KS1,KS2,max(limitPointsV),min(limitPointsV),x);
            ambientI=ambientLight(KA,Ia);
            specularI=specularLight(Pc,normals,C,KS,ncoeff,S,I0);
            diffuseI=diffuseLight(Pc,normals,KD,S,I0);
            Y(y,x,:)=ambientI+specularI+diffuseI;
        end
    end
    
    return;
    
%%
%The horizontal edge is the at the highest place
elseif ((yV(1)==yV(2) && yV(1)==ymax) || (yV(1)==yV(3) && yV(1)==ymax) || (yV(2)==yV(3) && yV(2)==ymax))
    xxV=xV(yV==ymax);
    normalsV=Vn(:,(yV==ymax));
    KAV=ka(:,(yV==ymax));
    KSV=ks(:,(yV==ymax));
    KDV=kd(:,(yV==ymax));
    [xxV,index]=sort(xxV);
    
    %Draw the horizontal edge
    
    for x=min(xxV):max(xxV)
        normals1=interp(normalsV(:,index(1)),normalsV(:,index(2)),xxV(1),xxV(2),x);
        ka1=interp(KAV(:,index(1)),KAV(:,index(2)),xxV(1),xxV(2),x);
        kd1=interp(KDV(:,index(1)),KDV(:,index(2)),xxV(1),xxV(2),x);
        ks1=interp(KSV(:,index(1)),KSV(:,index(2)),xxV(1),xxV(2),x);
        ambientI=ambientLight(ka1,Ia);
        specularI=specularLight(Pc,normals1,C,ks1,ncoeff,S,I0);
        diffuseI=diffuseLight(Pc,normals1,kd1,S,I0);
        Y(ymax,x,:)=ambientI+specularI+diffuseI;
    end

    for y=ymin:ymax-1
        
        %Find the active edges
        
        activeEdges(:)=(y>=ykminV & y<ykmaxV);
        
        %Find the active limit points
        
        limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
        mActiveKV=mKV(activeEdges);
        
        %Check if an edge is vertical (mK==Inf)
        
        if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
            [~,ia]=unique(xV,'stable');
            sameX=false(size(xV));
            sameX(ia)=true;
            limitPointsV(isnan(limitPointsV))=xV(~sameX);
        end
        
        %Measure the coefficients and normals that we will use at the interpolation
        
        normals1=interp(normalsV(:,index(2)),Vn(:,yV==ymin),ymax,ymin,y);
        normals2=interp(normalsV(:,index(1)),Vn(:,yV==ymin),ymax,ymin,y);
        KA1=interp(KAV(:,index(2)),ka(:,yV==ymin),ymax,ymin,y);
        KA2=interp(KAV(:,index(1)),ka(:,yV==ymin),ymax,ymin,y);
        KD1=interp(KDV(:,index(2)),kd(:,yV==ymin),ymax,ymin,y);
        KD2=interp(KDV(:,index(1)),kd(:,yV==ymin),ymax,ymin,y);
        KS1=interp(KSV(:,index(2)),ks(:,yV==ymin),ymax,ymin,y);
        KS2=interp(KSV(:,index(1)),ks(:,yV==ymin),ymax,ymin,y);
        
        
        %Draw every pixel of the current scanline
        
        for x=min(limitPointsV):max(limitPointsV)
            normals=interp(normals1,normals2,max(limitPointsV),min(limitPointsV),x);
            KA=interp(KA1,KA2,max(limitPointsV),min(limitPointsV),x);
            KD=interp(KD1,KD2,max(limitPointsV),min(limitPointsV),x);
            KS=interp(KS1,KS2,max(limitPointsV),min(limitPointsV),x);
            ambientI=ambientLight(KA,Ia);
            specularI=specularLight(Pc,normals,C,KS,ncoeff,S,I0);
            diffuseI=diffuseLight(Pc,normals,KD,S,I0);
            Y(y,x,:)=ambientI+specularI+diffuseI;
        end
    end
    
    return;
    
end

%%
%For every other triangle

for y=ymin:ymax-1
    
    %Find the active edges
    
    activeEdges(:)=(y>=ykminV & y<ykmaxV);
    
    %Find the active limit points
    
    limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
    mActiveKV=mKV(activeEdges);
    
    %y-coordinates of the active edges
    
    yActiveEdgeMin=ykminV(activeEdges);
    yActiveEdgeMax=ykmaxV(activeEdges);
    
    %Check for vertical lines
    
    if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
        [~,ia]=unique(xV,'stable');
        sameX=false(size(xV));
        sameX(ia)=true;
        limitPointsV(isnan(limitPointsV))=xV(~sameX);
    end
    
    %Measure the coefficients and normals that we will use at the interpolation
    
    if yActiveEdgeMin(1)==yActiveEdgeMin(2) && yActiveEdgeMax(1)~=yActiveEdgeMax(2)
        
        normals1=interp(Vn(:,(yV==yActiveEdgeMin(1))),Vn(:,(yV==yActiveEdgeMax(1))),yActiveEdgeMin(1),yActiveEdgeMax(1),y);
        normals2=interp(Vn(:,(yV==yActiveEdgeMin(1))),Vn(:,(yV==yActiveEdgeMax(2))),yActiveEdgeMin(1),yActiveEdgeMax(2),y);
        ka1=interp(ka(:,(yV==yActiveEdgeMin(1))),ka(:,(yV==yActiveEdgeMax(1))),yActiveEdgeMin(1),yActiveEdgeMax(1),y);
        ka2=interp(ka(:,(yV==yActiveEdgeMin(1))),ka(:,(yV==yActiveEdgeMax(2))),yActiveEdgeMin(1),yActiveEdgeMax(2),y);
        ks1=interp(ks(:,(yV==yActiveEdgeMin(1))),ks(:,(yV==yActiveEdgeMax(1))),yActiveEdgeMin(1),yActiveEdgeMax(1),y);
        ks2=interp(ks(:,(yV==yActiveEdgeMin(1))),ks(:,(yV==yActiveEdgeMax(2))),yActiveEdgeMin(1),yActiveEdgeMax(2),y);
        kd1=interp(kd(:,(yV==yActiveEdgeMin(1))),kd(:,(yV==yActiveEdgeMax(1))),yActiveEdgeMin(1),yActiveEdgeMax(1),y);
        kd2=interp(kd(:,(yV==yActiveEdgeMin(1))),kd(:,(yV==yActiveEdgeMax(2))),yActiveEdgeMin(1),yActiveEdgeMax(2),y);
        
    elseif yActiveEdgeMax(1)==yActiveEdgeMax(2) && yActiveEdgeMin(1)~=yActiveEdgeMin(2)
                
        normals1=interp(Vn(:,(yV==yActiveEdgeMax(1))),Vn(:,(yV==yActiveEdgeMin(1))),yActiveEdgeMax(1),yActiveEdgeMin(1),y);
        normals2=interp(Vn(:,(yV==yActiveEdgeMax(1))),Vn(:,(yV==yActiveEdgeMin(2))),yActiveEdgeMax(1),yActiveEdgeMin(2),y);
        ka1=interp(ka(:,(yV==yActiveEdgeMax(1))),ka(:,(yV==yActiveEdgeMin(1))),yActiveEdgeMax(1),yActiveEdgeMin(1),y);
        ka2=interp(ka(:,(yV==yActiveEdgeMax(1))),ka(:,(yV==yActiveEdgeMin(2))),yActiveEdgeMax(1),yActiveEdgeMin(2),y);
        ks1=interp(ks(:,(yV==yActiveEdgeMax(1))),ks(:,(yV==yActiveEdgeMin(1))),yActiveEdgeMax(1),yActiveEdgeMin(1),y);
        ks2=interp(ks(:,(yV==yActiveEdgeMax(1))),ks(:,(yV==yActiveEdgeMin(2))),yActiveEdgeMax(1),yActiveEdgeMin(2),y);
        kd1=interp(kd(:,(yV==yActiveEdgeMax(1))),kd(:,(yV==yActiveEdgeMin(1))),yActiveEdgeMax(1),yActiveEdgeMin(1),y);
        kd2=interp(kd(:,(yV==yActiveEdgeMax(1))),kd(:,(yV==yActiveEdgeMin(2))),yActiveEdgeMax(1),yActiveEdgeMin(2),y);
    end
    
     %Draw every pixel of the current scanline
     
     for x=min(limitPointsV):max(limitPointsV)
            normals=interp(normals1,normals2,limitPointsV(1),limitPointsV(2),x);
            KA=interp(ka1,ka2,limitPointsV(1),limitPointsV(2),x);
            KD=interp(kd1,kd2,limitPointsV(1),limitPointsV(2),x);
            KS=interp(ks1,ks2,limitPointsV(1),limitPointsV(2),x);
            ambientI=ambientLight(KA,Ia);
            specularI=specularLight(Pc,normals,C,KS,ncoeff,S,I0);
            diffuseI=diffuseLight(Pc,normals,KD,S,I0);
            Y(y,x,:)=ambientI+specularI+diffuseI;
      end
     
end
end