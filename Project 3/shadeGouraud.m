%View Project
%Author: Papas Ioannis 9218

function Y = shadeGouraud(p,Vn,Pc,C,S,ka,kd,ks,ncoeff,Ia,I0,X)

color=zeros(3,3);

for i=1:3
    ambientI=ambientLight(ka(:,i),Ia);
    specularI=specularLight(Pc,Vn(:,i),C,ks(:,i),ncoeff,S,I0);
    diffuseI=diffuseLight(Pc,Vn(:,i),kd(:,i),S,I0);
    color(i,:)=(ambientI+specularI+diffuseI)';
end

Y=triPaintGouraud(X,p',color);

end