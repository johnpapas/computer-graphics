%View Project
%Author: Papas Ioannis 9218

function I=diffuseLight(P,N,kd,S,I0)
% I=diffuseLight(P,N,kd,S,I0) calculates the diffuse light intensity
% INPUTS:
% P: 3x1 vector with the point coordinates
% N: 3x1 vector with the coordinates of the normal vector of the point 
% kd: 3x1 vector with the diffuse light coefficients
% S: 3xN matrix with the coordinates of every source
% I0: 3x1 vector with the intensity of the source
% OUTPUTS:
% I: 3x1 vector with the intensity at a point P

n=length(S(1,:)); %number of sources
I=zeros(3,1);
for iN=1:n
    d=norm(S(:,iN)-P);
    L=(S(:,iN)-P)/norm(S(:,iN)-P);
    fatt=1;     %/(d^2); <-we can use this for real results but the diffusion light will be almost 0

    NL=max(dot(L,N),0);

    I=(I0.*fatt'.*kd.*NL')+I; %Sums up all the intensities for the final result
end


end
