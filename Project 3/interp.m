%View Project
%Author: Papas Ioannis 9218

function interpolation = interp(A,B,a,b,x)
%interpolation = interp(A,B,a,b,x) is a function that finds the linear
%interpolation of a value for the point x interpolating from the 2 points
%a,b with values A,B where A,B vectors

if a<b
    interpolation=(A.*(b-x')+B.*(x'-a))/(b-a);
elseif a>b
    interpolation=(A.*(x'-b)+B.*(a-x'))/(a-b);
elseif a==b
    interpolation=A;
end

end