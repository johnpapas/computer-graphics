%View Project
%Author: Papas Ioannis 9218

function Im=photographObjectPhong(shader,f,C,K,u,bC,M,N,H,W,R,F,S,ka,kd,ks,ncoeff,Ia,I0)

%Find the normals
Normals=findVertNormals(R,F);
%Find the 2d coordinates
[P,D]=projectCameraKU(f,C,K,u,R);
P2d=rasterize(P,M,N,H,W);
%Create the canvas
Im=zeros(1200,1200,length(bC));
Im(:,:,1)=bC(1);
Im(:,:,2)=bC(2);
Im(:,:,3)=bC(3);

%Sort the triangle based on depth
triDepth=mean(D(F(:,:)')');
[sortedDepth, index]=sort(triDepth, 'descend');

%Paint every triangle with the right order. First those with the max depth
%until it reaches the min depth

if(shader==1)
    for i=1:length(sortedDepth)
        Pc=sum(R(:,F(:,index(i)))')/3;
        Im=shadeGouraud(P2d(:,F(:,index(i))),Normals(:,F(:,index(i))),Pc',C,S,ka(:,F(:,index(i))),kd(:,F(:,index(i))),ks(:,F(:,index(i))),ncoeff,Ia,I0,Im);
    end
elseif (shader==2)
    for i=1:length(sortedDepth)
        Pc=sum(R(:,F(:,index(i)))')/3;
        Im=shadePhong(P2d(:,F(:,index(i))),Normals(:,F(:,index(i))),Pc',C,S,ka(:,F(:,index(i))),kd(:,F(:,index(i))),ks(:,F(:,index(i))),ncoeff,Ia,I0,Im);
    end
end


end
