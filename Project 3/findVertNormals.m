%View Project
%Author: Papas Ioannis 9218

function Normals=findVertNormals(R,F)
% Normals=findVertNormals(R,F) calculates the normal vectors of each peak
% INPUTS:
% R: 3xr matrix with the peaks coordinates
% F: 3xT matrix with the combinations of matrix R that create a triangle
% OUTPUTS:
% N: 3xr matrix with the normal vector for each point of matrix R

n=length(R(1,:)); %number of peaks
Normals=zeros(3,n);
T=length(F(1,:)); %number of triangles

for iT=1:T
    %Calculate the normal vector for each triangle using the cross product
    %ABxAC and then add it to the normals of the 3 peaks used for the
    %triangle
    normalV=cross((R(:,F(2,iT))-R(:,F(1,iT))),(R(:,F(3,iT))-R(:,F(1,iT))));
    Normals(:,F(1,iT))=Normals(:,F(1,iT))+normalV;
    Normals(:,F(2,iT))=Normals(:,F(2,iT))+normalV;
    Normals(:,F(3,iT))=Normals(:,F(3,iT))+normalV;
end

%Normalize the normals so they have norm=1
for iR=1:n
    Normals(:,iR)=(Normals(:,iR)/norm(Normals(:,iR)));
end


end