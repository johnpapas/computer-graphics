%Filling Triangles Project
%Author: Papas Ioannis 9218

function Y = triPaintFlat(X,V,C)
% Y = triPaintFlat(X,V,C) fills an given triangle with the average color of
% its peaks
% INPUTS:
% X: matrix of the given canvas with size of M*N*3, with M,N the size and 3
%    represents the RGB
% V: matrix with the triangle peak coordinates with size of 3*2
% C: matrix with the color of the triangle peaks represented in the RGB
%    system with decimals with size of 3*3
% OUTPUTS:
% Y: matrix of the new canvas with the triangle filled

%Check if we dont have any triangle

if isempty(V)
    return;
end

%Variables
%Vector to be used to color every pixel
color=mean(C);
Y=X;
xV=V(:,1);
yV=V(:,2);

%Check if triangle is a horizontal line

if (yV(1)==yV(2) && yV(1)==yV(3))
    x=min(xV):max(xV);
    k=max(xV)-min(xV);
    Y(yV(1),x,:)=repmat(color,k+1,1);
    return;
end

%Check if triangle is a vertical line

if (xV(1)==xV(2) && xV(1)==xV(3))
    y=min(yV):max(yV);
    k=max(yV)-min(yV);
    Y(y,xV(1),:)=repmat(color,k+1,1);
    return;
end

%Filling Algorithm
ykminV=[min([yV(1) yV(2)]),min([yV(2) yV(3)]),min([yV(1) yV(3)])];
ykmaxV=[max([yV(1) yV(2)]),max([yV(2) yV(3)]),max([yV(1) yV(3)])];

ymin=min(ykminV);
ymax=max(ykmaxV);

%Check if an edge is horizontal and draw it

if((yV(1)==yV(2) && yV(1)==ymin) || (yV(1)==yV(3) && yV(1)==ymin) || (yV(2)==yV(3) && yV(2)==ymin))
    xxV=xV(yV==ymin);
    x=min(xxV):max(xxV);
    k=max(xxV)-min(xxV);
    
    %Draw every pixel
    
    Y(ymin,x,:)=repmat(color,k+1,1);
    ymin=ymin+1;
elseif ((yV(1)==yV(2) && yV(1)==ymax) || (yV(1)==yV(3) && yV(1)==ymax) || (yV(2)==yV(3) && yV(2)==ymax))
    xxV=xV(yV==ymax);
    x=min(xxV):max(xxV);
    k=max(xxV)-min(xxV);
    
    %Draw every pixel
    
    Y(ymax,x,:)=repmat(color,k+1,1);
end

activeEdges=false(3,1);

mKV=NaN(3,1);
mKV(1)=(yV(1)-yV(2))/(xV(1)-xV(2));
mKV(2)=(yV(2)-yV(3))/(xV(2)-xV(3));
mKV(3)=(yV(1)-yV(3))/(xV(1)-xV(3));

bV=yV-mKV.*xV;
limitPointsV=NaN(2,1);

for y=ymin:ymax-1
    
    %Find the active edges
    
    activeEdges(:)=(y>=ykminV & y<ykmaxV);
    
    mActiveKV=mKV(activeEdges);
    
    %Find the active limit points
            
    limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
    %Check for vertical lines
    
    if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
        [~,ia]=unique(xV,'stable');
        sameX=false(size(xV));
        sameX(ia)=true;
        limitPointsV(isnan(limitPointsV))=xV(~sameX);
    end
        
    %Draw every pixel
    
    x=min(limitPointsV):max(limitPointsV);
    k=max(limitPointsV)-min(limitPointsV); 
    Y(y,x,:)=repmat(color,k+1,1);

end

