%Filling Triangles Project
%Author: Papas Ioannis 9218

function color = colorInterp(A,B,a,b,x)
% color = colorInterp(A,B,a,b,x) measures the color for a specific point by
% interpolating its coordinates
% INPUTS:
% A,B: vectors of the colors of the two active limit points with size 3*1
% a,b: scalar variables of the x coordinates of the active limit points
% x: vector of the pixels which we want to find the color
% OUTPUTS:
% color: matrix with the color of every pixel from the given vector x with
%        size of 3*length(x)


if a<b
    color=(A.*(b-x')+B.*(x'-a))/(b-a);
elseif a>b
    color=(A.*(x'-b)+B.*(a-x'))/(a-b);
elseif a==b
    color=A;
end

end