%Filling Triangles Project
%Author: Papas Ioannis 9218

function I=paintObject(X,V,F,C,D,painter)
% I = paintObject(X,V,F,C,D,painter) fills a given canvas which has
% triangles given by their peak coordinates
% INPUTS:
% X: matrix of the given canvas with size of M*N*3, with M,N the size and 3
%    represents the RGB
% V: matrix with all the peak coordinates in the canvas with size of L*2,
%    where L the number of points we have
% F: matrix which tells us the points used as peaks for every triangle in
%    every row with a size of K*3, where K the number of triangles we have
% C: matrix with the color of the peaks represented in the RGB
%    system with decimals with size of L*3
% D: matrix with the depth of every point before the projection of the
%    object with size L*1
% painter: a boolean variable that indicates if we will use the flat or the
%          Gouraud filling method
% OUTPUTS:
% I: matrix of the new canvas with all the triangles filled

%Find the mean depth of every triangle and sort it

triDepth=mean(D(F(:,:)'));
[sortedDepth, index]=sort(triDepth, 'descend');

%Paint every triangle with the right order. First those with the max depth
%until it reaches the min depth

if painter==0
    for i=1:length(sortedDepth)
        X=triPaintFlat(X,V(F(index(i),:),:),C(F(index(i),:),:));
    end
elseif painter==1
    for i=1:length(sortedDepth)
        X=triPaintGouraud(X,V(F(index(i),:),:),C(F(index(i),:),:));
    end
end

%Save the final canvas

I=X;

end