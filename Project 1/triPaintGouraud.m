%Filling Triangles Project
%Author: Papas Ioannis 9218

function Y = triPaintGouraud(X,V,C)
% Y = triPaintGouraud(X,V,C) fills an given triangle with color measured
% with the Gouraud method interpolating its peaks
% INPUTS:
% X: matrix of the given canvas with size of M*N*3, with M,N the size and 3
%    represents the RGB
% V: matrix with the triangle peak coordinates with size of 3*2
% C: matrix with the color of the triangle peaks represented in the RGB
%    system with decimals with size of 3*3
% OUTPUTS:
% Y: matrix of the new canvas with the triangle filled

%Check if we dont have any triangle

if isempty(V)
    return;
end

%Variables
Y=X;
xV=V(:,1);
yV=V(:,2);

%Check if triangle is a horizontal line

if (yV(1)==yV(2) && yV(1)==yV(3))
    x=min(xV):max(xV);
    Y(yV(1),x,:)=colorInterp(C(1,:), C(2,:), min(xV), max(xV), x);
    return;
end

%Check if triangle is a vertical line

if (xV(1)==xV(2) && xV(1)==xV(3))
    y=min(yV):max(yV);
    Y(y,xV(1),:)=colorInterp(C(1,:), C(2,:), min(yV), max(yV), y);
    return;
end

%Filling Algorithm

ykminV=[min([yV(1) yV(2)]),min([yV(2) yV(3)]),min([yV(1) yV(3)])];
ykmaxV=[max([yV(1) yV(2)]),max([yV(2) yV(3)]),max([yV(1) yV(3)])];

ymin=min(ykminV);
ymax=max(ykmaxV);

activeEdges=false(3,1);

mKV=NaN(3,1);
mKV(1)=(yV(1)-yV(2))/(xV(1)-xV(2));
mKV(2)=(yV(2)-yV(3))/(xV(2)-xV(3));
mKV(3)=(yV(1)-yV(3))/(xV(1)-xV(3));

bV=yV-mKV.*xV;

%Check if the triangle has an horizontal edge and draw it

if((yV(1)==yV(2) && yV(1)==ymin) || (yV(1)==yV(3) && yV(1)==ymin) || (yV(2)==yV(3) && yV(2)==ymin))
    xxV=xV(yV==ymin);
    colorV=C((yV==ymin),:);
    [xxV,index]=sort(xxV);
    
    %Draw the horizontal edge
    
    x=min(xxV):max(xxV);
    Y(ymin,x,:)=colorInterp(colorV(index(1),:), colorV(index(2),:), xxV(1), xxV(2), x);
    ymin=ymin+1;
    for y=ymin:ymax-1
        
        %Find the active edges
        
        activeEdges(:)=(y>=ykminV & y<ykmaxV);
        
        %Find the active limit points
        
        limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
        mActiveKV=mKV(activeEdges);
        
        %Check if an edge is vertical (mK==Inf)
        
        if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
            [~,ia]=unique(xV,'stable');
            sameX=false(size(xV));
            sameX(ia)=true;
            limitPointsV(isnan(limitPointsV))=xV(~sameX);
        end
        
        %Measure the colors that we will use at the interpolation
        
        i1=colorV(index(2),:);
        i2=colorV(index(1),:);
        i3=C((yV==ymax),:);
        color1=(i1*(ymax-y)+i3*(y-ymin))/(ymax-ymin);
        color2=(i2*(ymax-y)+i3*(y-ymin))/(ymax-ymin);
        
        %Draw every pixel
        
        x=min(limitPointsV):max(limitPointsV);
        Y(y,x,:)=colorInterp(color1, color2, max(limitPointsV), min(limitPointsV), x);
    end
    
    return;
    
elseif ((yV(1)==yV(2) && yV(1)==ymax) || (yV(1)==yV(3) && yV(1)==ymax) || (yV(2)==yV(3) && yV(2)==ymax))
    xxV=xV(yV==ymax);
    colorV=C((yV==ymax),:);
    [xxV,index]=sort(xxV);
    
    %Draw the horizontal edge
    
    x=min(xxV):max(xxV);
    Y(ymax,x,:)=colorInterp(colorV(index(1),:), colorV(index(2),:), xxV(1), xxV(2), x);
    for y=ymin:ymax-1
        
        %Find the active edges
        
        activeEdges(:)=(y>=ykminV & y<ykmaxV);
        
        %Find the active limit points
        
        limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
        mActiveKV=mKV(activeEdges);
        
        %Check if an edge is vertical (mK==Inf)
        
        if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
            [~,ia]=unique(xV,'stable');
            sameX=false(size(xV));
            sameX(ia)=true;
            limitPointsV(isnan(limitPointsV))=xV(~sameX);
        end
        
        %Measure the colors that we will use at the interpolation
        
        i1=colorV(index(2),:);
        i2=colorV(index(1),:);
        i3=C((yV==ymin),:);
        color1=(i3*(ymax-y)+i1*(y-ymin))/(ymax-ymin);
        color2=(i3*(ymax-y)+i2*(y-ymin))/(ymax-ymin);
        
        %Draw every pixel
        
        x=min(limitPointsV):max(limitPointsV);
        Y(y,x,:)=colorInterp(color1, color2, max(limitPointsV), min(limitPointsV), x);
    end
    
    return;
    
end


for y=ymin:ymax-1
    
    %Find the active edges
    
    activeEdges(:)=(y>=ykminV & y<ykmaxV);
    
    %Find the active limit points
    
    limitPointsV=round((y-bV(activeEdges))./mKV(activeEdges));
    
    mActiveKV=mKV(activeEdges);
    
    %y-coordinates of the active edges
    
    yActiveEdgeMin=ykminV(activeEdges);
    yActiveEdgeMax=ykmaxV(activeEdges);
    
    %Check for vertical lines
    
    if 1/mActiveKV(1)==0 || 1/mActiveKV(2)==0
        [~,ia]=unique(xV,'stable');
        sameX=false(size(xV));
        sameX(ia)=true;
        limitPointsV(isnan(limitPointsV))=xV(~sameX);
    end
    
    %Measure the colors that we will use at the interpolation
    
    if yActiveEdgeMin(1)==yActiveEdgeMin(2) && yActiveEdgeMax(1)~=yActiveEdgeMax(2)
        i1=C((yV==yActiveEdgeMin(1)),:);
        i2=C((yV==yActiveEdgeMax(1)),:);
        i3=C((yV==yActiveEdgeMax(2)),:);
        color1=(i1*(yActiveEdgeMax(1)-y)+i2*(y-yActiveEdgeMin(1)))/(yActiveEdgeMax(1)-yActiveEdgeMin(1));
        color2=(i1*(yActiveEdgeMax(2)-y)+i3*(y-yActiveEdgeMin(1)))/(yActiveEdgeMax(2)-yActiveEdgeMin(1));
    elseif yActiveEdgeMax(1)==yActiveEdgeMax(2) && yActiveEdgeMin(1)~=yActiveEdgeMin(2)
        i1=C((yV==yActiveEdgeMax(1)),:);
        i2=C((yV==yActiveEdgeMin(1)),:);
        i3=C((yV==yActiveEdgeMin(2)),:);
        color1=(i1*(y-yActiveEdgeMin(1))+i2*(yActiveEdgeMax(1)-y))/(yActiveEdgeMax(1)-yActiveEdgeMin(1));
        color2=(i1*(y-yActiveEdgeMin(2))+i3*(yActiveEdgeMax(1)-y))/(yActiveEdgeMax(1)-yActiveEdgeMin(2));
    end
    
     %Draw every pixel
     
     x=min(limitPointsV):max(limitPointsV);
     Y(y,x,:)=colorInterp(color1, color2, limitPointsV(1), limitPointsV(2), x);
     
end