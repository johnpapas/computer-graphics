%Transforms and projections Project
%Author: Papas Ioannis 9218

function Prast=rasterize(P,M,N,H,W)
% Prast=rasterize(P,M,N,H,W) calculates the points which are seen and
% transforms their coordinates at the new canvas size given the camera
% panel size and the canvas size
% INPUTS:
% P: 2xn matrix with 2D coordinates of the points after the perspective 
%    projection
% M: scalar indicating the length of the canvas in pixels
% N: scalar indicating the width of the canvas in pixels
% H: scalar indicating the height of the camera panel
% W: scalar indicating the width of the camera panel
% OUTPUTS:
% Prast: 2xn matrix with the coordinates of the points based at the canvas
%        size

Prast(1,:)=floor(((P(1,:)+(W/2))/W)*N);
Prast(2,:)=floor((1-(P(2,:)+(H/2))/H)*M);

end