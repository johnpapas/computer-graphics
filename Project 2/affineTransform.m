%Transforms and projections Project
%Author: Papas Ioannis 9218

function cq=affineTransform(cp,R,ct)
% cq=affineTransform(cp,R,ct) makes an affine transformation of the given
% coordinates cp, first rotating and then moving it by ct
% INPUTS:
% cp: 3xn matrix with initial coordinates of our points
% R: 3x3 rotation transform matrix
% ct: 3x1 vector with coordinates of moving vector
% OUTPUTS:
% cq: 3xn matrix with the new coordinates of our points

temp=R*cp;
cq=temp+ct;

end