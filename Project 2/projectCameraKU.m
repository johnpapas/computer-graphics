%Transforms and projections Project
%Author: Papas Ioannis 9218

function [P,D]=projectCameraKU(w,cv,ck,cu,p)
% [P,D]=projectCamera(w,cv,cx,cy,p) calculates the 2D coordinates of a 3D
% point. The new coordinates are the coordinates after the perspective
% projection of the object. It also calculates the depth of every point
% used for the layers.
% INPUTS:
% w: distance between camera panel and centre
% cv: 3x1 vector of the coordinates of the camera center
% ck: 3x1 vector of the coordinates of the center where the camera targets
% cu: 3x1 unary vector of the up-vector which indicates the up position of
%     the camera
% p: 3xn vector of the coordinates of our points
% OUTPUTS:
% P: 2xn matrix with the 2D coordinates of the points after the perspective
%    projection
% D: 1xn vector with the depths of the points after the perspective
%    projection

cz=(cv-ck)/norm(cv-ck);
t=cu-(cu'*cz)*cz;
cy=t/norm(t);
cx=cross(cy,cz);
[P,D]=projectCamera(w,cv,cx,cy,p);


end

