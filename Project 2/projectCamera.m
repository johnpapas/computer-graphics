%Transforms and projections Project
%Author: Papas Ioannis 9218

function [P,D]=projectCamera(w,cv,cx,cy,p)
% [P,D]=projectCamera(w,cv,cx,cy,p) calculates the 2D coordinates of a 3D
% point. The new coordinates are the coordinates after the perspective
% projection of the object. It also calculates the depth of every point
% used for the layers.
% INPUTS:
% w: distance between camera panel and centre
% cv: 3x1 vector of the coordinates of the camera center
% cx: 3x1 vector of the coordinates of x-axis of the camera system
%     based on the WCS
% cy: 3x1 vector of the coordinates of y-axis of the camera system
%     based on the WCS
% p: 3xn vector of the coordinates of our points
% OUTPUTS:
% P: 2xn matrix with the 2D coordinates of the points after the perspective
%    projection
% D: 1xn vector with the depths of the points after the perspective
%    projection

cz=cross(cx,cy);
cq=systemTransform(p,cx,cy,cz,cv);
P(1,:)=w*cq(1,:)./(-cq(3,:));
P(2,:)=w*cq(2,:)./(-cq(3,:));
D=-cq(3,:);


end