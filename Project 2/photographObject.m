%Transforms and projections Project
%Author: Papas Ioannis 9218

function [P2d,D]=photographObject(p,M,N,H,W,w,cv,cK,cu)
%[P2d,D]=photographObject(p,M,N,H,W,w,cv,cK,cu) simulates all the way of
%photograping an object given the 3d points, the canvas to be projected
%size, the camera panel size, and the parameters of the camera system
% INPUTS:
% p: 3xn vector of the coordinates of our points
% M: scalar indicating the length of the canvas in pixels
% N: scalar indicating the width of the canvas in pixels
% H: scalar indicating the height of the camera panel
% W: scalar indicating the width of the camera panel
% w: distance between camera panel and centre
% cv: 3x1 vector of the coordinates of the camera center
% cx: 3x1 vector of the coordinates of x-axis of the camera system
%     based on the WCS
% cy: 3x1 vector of the coordinates of y-axis of the camera system
%     based on the WCS
% OUTPUTS:
% Prast: 2xn matrix with the coordinates of the points based at the canvas
%        size
% D: 1xn vector with the depths of the points after the perspective
%    projection



[P,D]=projectCameraKU(w,cv,cK,cu,p);
P2d=rasterize(P,M,N,H,W);


end