%Transforms and projections Project
%Author: Papas Ioannis 9218

function dp=systemTransform(cp,b1,b2,b3,c0)
% dp=systemTransform(cp,b1,b2,b3,c0) transforms the cp coordinates to the
% dp coordinates having new base b1,b2,b3 and new
% INPUTS:
% cp: 3xn matrix with initial coordinates of our points
% b1,b2,b3: 3x1 new axises
% c0: 3x1 vector with coordinates of moving vector
% OUTPUTS:
% dp: 3xn matrix with the new coordinates of our points

L=[b1 b2 b3];
dp=inv(L)*(cp-c0);

end

