%Transforms and projections Project
%Author: Papas Ioannis 9218

function R=rotationMatrix(theta,u)
% R=rotationMatrix(theta,u) calculates the transformation matrix R which is
% used to rotate an given vector
% INPUTS:
% theta: angle of rotation (rad)
% u: 3x1 vector of the rotation axis
% OUTPUTS:
% R: 3x3 rotation transform matrix

oc=u*u';
op=eye(3);
uop=[cross(u,op(:,1)),cross(u,op(:,2)),cross(u,op(:,3))];
R=(1-cos(theta))*oc+cos(theta)*op+sin(theta)*uop;


end